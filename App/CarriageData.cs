﻿using System;

namespace App
{
    public class CarriageData
    {
        public CarriageData(
            string name,
            int countInPark,
            int passangerCount,
            int fastTrain,
            int passangerTrain,
            int cargoTrain,
            int industrialTrain,
            int cargoAndPassangerTrain
            )
        {
            Name = name;
            CountInPark = countInPark.ToString();
            PassengerCount = passangerCount.ToString();
            FastTrain = fastTrain.ToString();
            PassengerTrain = passangerTrain.ToString();
            CargoTrain = cargoTrain.ToString();
            IndustrialTrain = industrialTrain.ToString();
            CargoAndPassengerTrain = cargoAndPassangerTrain.ToString();
        }

        public string GetDataByType(CarriageType type)
        {
            switch (type)
            {
                case CarriageType.Fast: return FastTrain;
                case CarriageType.Passenger: return PassengerTrain;
                case CarriageType.Cargo: return CargoTrain;
                case CarriageType.Industrial: return IndustrialTrain;
                case CarriageType.CargoAndPassenger: return CargoAndPassengerTrain;
                default: throw new Exception($"Unsupported {nameof(CarriageType)}");
            }
        }

        [ColumnName("Вагон")]
        public string Name { get; set; }
        [ColumnName("Парк вагонів")]
        public string CountInPark { get; set; }
        [ColumnName("Кількість пасажирів")]
        public string PassengerCount { get; set; }
        [ColumnName("Швидкий потяг")]
        public string FastTrain { get; set; }
        [ColumnName("Пасажирський потяг")]
        public string PassengerTrain { get; set; }
        [ColumnName("Вантажний  потяг")]
        public string CargoTrain { get; set; }
        [ColumnName("Промисловий потяг")]
        public string IndustrialTrain { get; set; }
        [ColumnName("Вантажно-пасажирський потяг")]
        public string CargoAndPassengerTrain { get; set; }
    }
    public class ColumnNameAttribute : Attribute
    {
        public ColumnNameAttribute(string Name) { this.Name = Name; }
        public string Name { get; set; }
    }
    public enum CarriageType
    {
        Fast=3,
        Passenger,
        Cargo,
        Industrial,
        CargoAndPassenger,
    }
}
