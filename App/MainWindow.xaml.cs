﻿using App;
using Simplex;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppSpace
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IDictionary<CarriageType, bool> Mapping = new Dictionary<CarriageType, bool>
        {
            { CarriageType.Fast, true },
            { CarriageType.Passenger, true },
            { CarriageType.Cargo, false },
            { CarriageType.Industrial, false },
            { CarriageType.CargoAndPassenger, false },
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow1_Initialized(object sender, EventArgs e)
        {
            Table.ItemsSource = new List<CarriageData> {
             new CarriageData("Багажний", 12, 0, 1, 1, 0, 0, 0),
             new CarriageData("Поштовий", 18, 0, 1, 0, 0, 0, 0),
             new CarriageData("Жорсткий", 89, 58, 5, 8, 0, 0, 0),
             new CarriageData("Купейний", 79, 40, 6, 4, 0, 0, 0),
             new CarriageData("М’який", 35, 32, 4, 2, 0, 0, 0),
            };
        }
        private void Solve_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var typesCount = Mapping.Values.Where(e => e).Count();

                if (typesCount < 2)
                    throw new InvalidDataException("Не може бути менше 2 потягів!");

                var list = (List<CarriageData>)Table.ItemsSource;

                if (list.Count() < 2)
                    throw new InvalidDataException("Не може бути менше 3 видів вагонів!");

                var table = new double[list.Count() + 1, typesCount + list.Count() + 2];

                for (int i = 0; i < list.Count(); i++)
                {
                    table[i, 0] = GetIntPositive(list[i].CountInPark);
                    int cursor = 1;
                    foreach (var key in Mapping.Keys)
                    {
                        if (Mapping[key])
                        {
                            table[i, cursor++] = GetIntPositive(list[i].GetDataByType(key));
                        }
                    }
                    for (int k = typesCount + 1; k < table.GetLength(1); k++)
                    {
                        table[i, k] = (i + typesCount + 1 == k) ? 1 : 0;
                    }
                }
                table[table.GetLength(0) - 1, 0] = GetIntPositive(MaxTrains.Text);
                for (int i = 1; i < table.GetLength(1) - 1; i++)
                {
                    table[table.GetLength(0) - 1, i] = i <= typesCount ? 1 : 0;
                }
                table[table.GetLength(0) - 1, table.GetLength(1) - 1] = 1;

                var basis = (new int[list.Count() + 1]).Select((e, i) => i + typesCount + 1).ToArray();
                var includedKeys = Mapping.Keys.Where(k => Mapping[k]).ToArray();
                int keyCursor = 0;
                var f = (new int[table.GetLength(1)]).Select((e, i) =>
                {
                    if (i == 0) return 0;
                    if (i <= typesCount)
                    {
                        var sum = list.Select((el, index) => GetIntPositive(
                            el.GetDataByType(includedKeys[keyCursor])) * GetIntPositive(el.PassengerCount)
                        ).Sum();
                        keyCursor++;
                        return (double)sum;

                    }
                    return 0;
                }).ToArray();
                var simplex = new AreasSimplex(table, basis, f);
                var res = simplex.Solve();
                var resultText = $"Можливо перевезти {res[0]} пасажирів\n";
                keyCursor = 1;
                foreach (var item in Mapping.Keys.Where(k => Mapping[k]))
                {
                    resultText += $"{Table.Columns[(int)item].Header}: {res[keyCursor++]} шт.\n";
                }

                MessageBox.Show(resultText, "Готово!", MessageBoxButton.OK, MessageBoxImage.Information);
                simplex.SaveData();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Помилка в ведених даних!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private int GetIntPositive(string val)
        {
            if (int.TryParse(val, out int res))
            {
                if (res < 0)
                    throw new InvalidDataException("Значення змінної не може бути мешне 0");
                return res;
            }
            throw new InvalidDataException("Значення змінної повинно бути числом!");
        }

        private void UpdateColumns()
        {
            UpdateColumn(CheckTrain_Fast.IsChecked, CarriageType.Fast);
            UpdateColumn(CheckTrain_Pass.IsChecked, CarriageType.Passenger);
            UpdateColumn(CheckTrain_Cargo.IsChecked, CarriageType.Cargo);
            UpdateColumn(CheckTrain_Industrial.IsChecked, CarriageType.Industrial);
            UpdateColumn(CheckTrain_PassAndCargo.IsChecked, CarriageType.CargoAndPassenger);
            CollectionViewSource.GetDefaultView(Table.ItemsSource).Refresh();
        }

        private void UpdateColumn(bool? isChecked, CarriageType carriageType)
        {
            Table.Columns[(int)carriageType].Visibility = isChecked == true ? Visibility.Visible : Visibility.Collapsed;
            Mapping[carriageType] = isChecked == true;
        }


        private void Table_AutoGeneratedColumns(object sender, EventArgs e)
        {
            UpdateColumns();
        }

        private void AddVagon_Click(object sender, RoutedEventArgs e)
        {
            ((List<CarriageData>)Table.ItemsSource).Add(new CarriageData("", 0, 0, 0, 0, 0, 0, 0));
            CollectionViewSource.GetDefaultView(Table.ItemsSource).Refresh();
        }

        private void RemoveVagon_Click(object sender, RoutedEventArgs e)
        {
            var list = (List<CarriageData>)Table.ItemsSource;
            if (list.Count() < 2) return;
            list.RemoveAt(list.Count - 1);
            CollectionViewSource.GetDefaultView(Table.ItemsSource).Refresh();
        }

        private void AddColumn_Click(object sender, RoutedEventArgs e)
        {
            Table.Columns[3].Visibility = Visibility.Collapsed;
            CollectionViewSource.GetDefaultView(Table.ItemsSource).Refresh();
        }

        private void CheckTrain_Click(object sender, RoutedEventArgs e)
        {
            UpdateColumns();
        }

        private void Table_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            var desc = e.PropertyDescriptor as PropertyDescriptor;
            var att = desc.Attributes[typeof(ColumnNameAttribute)] as ColumnNameAttribute;
            if (att != null)
            {
                e.Column.Header = att.Name;
            }
        }
    }

    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }
}
