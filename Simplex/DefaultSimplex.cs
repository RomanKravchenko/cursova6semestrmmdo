﻿using Spire.Xls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Simplex
{
    public class DefaultSimplex
    {
        protected double[,] CurrentTable;
        protected int[] Basis;
        protected double[] F;
        protected double[] Delta;
        protected List<string[,]> Data;
        public DefaultSimplex(double[,] table, int[] basis, double[] f)
        {
            F = f;
            Basis = basis;
            CurrentTable = Helpers.Transpose(table);
            Delta = new double[CurrentTable.GetLength(0)];
            Data = new List<string[,]>();
        }

        public virtual double[] Solve()
        {
            CalculateDelta();
            CommitData("Initial");
            while (!CheckPlan())
            {
                var (r, c) = GetSolveColumnRow();
                Iteratе(r,c);
                CalculateDelta();
                CommitData("Simplex");
            }
            return GetResult();
        }

        protected (int, int) GetSolveColumnRow()
        {
            int columnIndex = Array.IndexOf(Delta.Skip(1).ToArray(), Delta.Skip(1).Min()) + 1;
            var columnHelper = (new double[CurrentTable.GetLength(1)])
                .Select((e, index) => CurrentTable[0, index] / CurrentTable[columnIndex, index])
                .Select(e => e > 0 ? e : int.MaxValue).ToArray();
            int rowIndex = Array.IndexOf(columnHelper, columnHelper.Min());
            return (rowIndex, columnIndex);
        }

        protected void Iteratе(int rowIndex, int columnIndex)
        {
            Basis[rowIndex] = columnIndex;

            double[,] newTable = new double[CurrentTable.GetLength(0), CurrentTable.GetLength(1)];

            for (int i = 0; i < CurrentTable.GetLength(0); i++)
            {
                for (int k = 0; k < CurrentTable.GetLength(1); k++)
                {
                    if (k == rowIndex)
                    {
                        newTable[i, k] = CurrentTable[i, k] / CurrentTable[columnIndex, rowIndex];
                    } else
                    {
                        newTable[i, k] = CurrentTable[i, k] - (CurrentTable[i, rowIndex] * CurrentTable[columnIndex, k] / CurrentTable[columnIndex, rowIndex]);
                    }
                }
            }
            CurrentTable = newTable;
        }
        //to do third check
        protected bool CheckPlan()
        {
            for (int i = 1; i < Delta.Length; i++)
            {
                if (Delta[i] < 0) return false;
            }
            return true;
        }

        protected void CalculateDelta()
        {
            for (int i = 0; i < CurrentTable.GetLength(0); i++)
            {
                double sum = -1 * F[i];
                for (int k = 0; k < CurrentTable.GetLength(1); k++)
                {
                    var koef = F[Basis[k]];
                    sum += koef  * CurrentTable[i, k];
                }
                Delta[i] = sum;
            }
        }

        protected double[] GetResult()
        {
            var res = new double[CurrentTable.GetLength(0)];
            res[0] = Delta[0];

            for (int i = 1; i < res.Length; i++)
            {
                res[i] = Basis.ToList().Contains(i) ? CurrentTable[0, Array.IndexOf(Basis, i)] : 0;
            }

            return res;
        }

        protected void CommitData(string name)
        {
            var data = new string[CurrentTable.GetLength(1)+3, CurrentTable.GetLength(0)+1];
            data[0, 0] = name;
            for (int i = 0; i < CurrentTable.GetLength(0); i++)
            {
                data[0, i + 1] = F[i].ToString(".00");
                data[1, i + 1] = "A" + i;
                data[data.GetLength(0) - 1, i+1] = Delta[i].ToString(".00");
            }
            data[1, 0] = "B";
            for (int i = 0; i < CurrentTable.GetLength(1); i++)
            {
                data[i + 2, 0] = "X" + Basis[i];
                for (int k = 0; k < CurrentTable.GetLength(0); k++)
                {
                    data[i + 2, k + 1] = CurrentTable[k, i].ToString(".00");
                }
            }
            data[data.GetLength(0) - 1, 0] = "D";
            Data.Add(data);
        }

        public void SaveData()
        {
            Workbook workbook = new Workbook();
            workbook.CreateEmptySheets(Data.Count());
            for (int i = 0; i < Data.Count(); i++)
            {
                Worksheet sheet = workbook.Worksheets[i];
                sheet.InsertArray(Data[i], 1, 1);
                sheet.Name = Data[i][0, 0] + "-" + i;
            }

            DateTime dateTime = DateTime.Now;
            var path = @$"D:\mmdo\log_{dateTime.ToShortDateString()}_{dateTime.ToFileTime()}.xls";
            workbook.SaveToFile(path, ExcelVersion.Version2013);

        }
    }
}
