﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplex
{
    public class AreasSimplex: DefaultSimplex
    {

        public AreasSimplex(double[,] table, int[] basis, double[] f) : base(table, basis, f)
        {
        } 

        public override double[] Solve()
        {
            var res = base.Solve();
            if (!isSolved())
                SolveWithRestriction(res, true);
            while (!isSolved())
            {
                SolveWithRestriction(GetResult(), false);
            }
            return GetResult().Select((e, i) => Math.Abs(Math.Round(e))).ToArray();
        }

        protected bool isSolved()
        {
            var res = GetResult();
            for (int i = 1; i < res.Length; i++)
            {
                if (F[i] != 0 && Math.Abs(res[i] - Math.Round(res[i])) > 0.01)
                {
                    return false;
                }
            }

            return true;
        }

        protected void SolveWithRestriction(double[] res, bool changeF)
        {
            int index = 0;
            double value = 0;
            for (int i = 1; i < res.Length; i++)
            {
                var current = res[i] - Math.Floor(res[i]);
                if (F[i] != 0 && current > value)
                {
                    value = current;
                    index = i;
                }
            }
            var rowIndex = Array.IndexOf(Basis, index);
            var newTable = new double[CurrentTable.GetLength(0) + 1, CurrentTable.GetLength(1) + 1];
            for (int k = 0; k < CurrentTable.GetLength(1); k++)
            {
                for (int i = 0; i < CurrentTable.GetLength(0); i++)
                {
                    newTable[i, k] = CurrentTable[i, k];
                }
                newTable[CurrentTable.GetLength(0), k] = 0;
            }
            for (int i = 0; i < CurrentTable.GetLength(0); i++)
            {
                var val = CurrentTable[i, rowIndex];
                var newVal = -(val - Math.Round(val, MidpointRounding.ToNegativeInfinity));
                newTable[i, CurrentTable.GetLength(1)] = newVal;
            }
            newTable[CurrentTable.GetLength(0), CurrentTable.GetLength(1)] = 1;
            var newBasis = new int[Basis.Length + 1];
            Basis.CopyTo(newBasis, 0);
            newBasis[Basis.Length] = F.Length;
            var newF = new double[F.Length + 1];
            F.CopyTo(newF, 0);
            newF[F.Length] = 0;
            CurrentTable = newTable;
            Basis = newBasis;
            F = changeF ? newF.Select(f => -1 * f).ToArray() : newF;
            Delta = new double[Delta.Length + 1];
            CalculateDelta();
            CommitData("PreAreasSimplex");
            do
            {
                var (r, c) = GetAreasSolveColumnRow();
                Iteratе(r, c);
                CalculateDelta();
                CommitData("AreasSimplex");
            } while (GetResult().ToList().Skip(1).Any(e => e < 0));
        }
        
        protected (int, int) GetAreasSolveColumnRow()
        {
            int rowIndex = 0;
            double max = 0;
            for (int k = 0; k < CurrentTable.GetLength(1); k++)
            {
                if (CurrentTable[0, k] < 0 && Math.Abs(CurrentTable[0, k]) >= max)
                {
                    rowIndex = k;
                    max = Math.Abs(CurrentTable[0, k]);
                }
            }
            int columnIndex = 0;
            double min = double.MaxValue;
            for (int i = 1; i < CurrentTable.GetLength(0); i++)
            {
                if (CurrentTable[i, rowIndex] < 0)
                {
                    var local = Delta[i] / CurrentTable[i, rowIndex];
                    if (local < min)
                    {
                        min = local;
                        columnIndex = i;
                    }
                }
            }

            return (rowIndex, columnIndex);
        }
    }
}
