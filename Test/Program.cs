﻿using Simplex;
using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var table = new double[,]
                {
                    { 12, 1, 1, 1, 0, 0, 0, 0, },
                    { 18, 1, 2, 0, 1, 0, 0, 0, },
                    { 89, 5, 8, 0, 0, 1, 0, 0, },
                    { 79, 6, 4, 0, 0, 0, 1, 0, },
                    { 35, 4, 6, 0, 0, 0, 0, 1, },
                };
            var basis = new int[] { 3, 4, 5, 6, 7 };
            var f = new double[] { 0, 658, 816, 0, 0, 0, 0, 0 };
            var res = new AreasSimplex(table, basis, f).Solve();
            Console.WriteLine(string.Join(", ", res));
        }
    }
}
